import * as React from 'react';
import { Text, View } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import Home from './Components/Home';
import Ionicons from 'react-native-vector-icons/Ionicons';
import Users from './Components/Users.js';
import Lists from './Components/Lists.js';
import Login from './Components/Login.js';
import Global from './global.js';
 
const Tab = createBottomTabNavigator(); 
const Stack = createNativeStackNavigator();

export default function App() {
  // const [userId, setUserId] = React.useState(global.id_utilisateur);
  const [bearerToken, setBearerToken] = React.useState(global.bearerToken);

  // // Update userId state when global.id_utilisateur changes
  // React.useEffect(() => {
  //   setUserId(global.id_utilisateur);
  // }, [global.id_utilisateur]);

  // const changeIdUtilisateur = (id) => {
  //   setUserId(global.id_utilisateur);
  // };

  // Update userId state when global.bearerToken changes
  React.useEffect(() => {
    setBearerToken(global.bearerToken);
  }, [global.bearerToken]);

  const changeBearerToken = (token) => {
    setBearerToken(global.bearerToken);
  };

  return (
    <NavigationContainer>
      {bearerToken != null ? (
        <Tab.Navigator
          screenOptions={({ route }) => ({
            tabBarIcon: ({ focused, color, size }) => {
              let iconName;

              console.log(route.name);

              if (route.name === 'Accueil') {
                iconName = focused ? 'ios-home' : 'ios-home';
              } else if (route.name === 'Compte') {
                iconName = focused ? 'ios-person' : 'ios-person';
              } else {
                iconName = focused ? 'ios-list' : 'ios-list';
              }

              // Centrer verticalement les icônes
              return (
                <View style={{ alignItems: 'center' }}>
                  <Ionicons name={iconName} size={size} color={color} />
                </View>
              );
            },
            tabBarActiveTintColor: '#41B8D5',
            tabBarInactiveTintColor: 'gray',
          })}
        >
          <Tab.Screen options={{ tabBarLabel: '', headerShown: false}} name="Liste" component={Lists} />
          <Tab.Screen options={{ tabBarLabel: '', headerShown: false}} name="Accueil" component={Home} />
          <Tab.Screen options={{ tabBarLabel: '', headerShown: false}} name="Compte" component={Users} />
        </Tab.Navigator>
      ) : (
        <Stack.Navigator>
          <Stack.Screen name="Login" options={{headerShown: false}}>
            {() => <Login onLogin={changeBearerToken} />}
          </Stack.Screen>
        </Stack.Navigator>
      )}
    </NavigationContainer>
  );
}