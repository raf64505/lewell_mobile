import React, { useEffect, useState } from 'react';
import { View, Text, StyleSheet } from 'react-native';
import { LineChart } from 'react-native-chart-kit';
import stat from '../bdd_json/stat.json';
import puits from '../bdd_json/puit.json';

const Home = () => {
 
  const [graphData, setGraphData] = useState(null);

  useEffect(() => {
    const fetchDataForPuits = () => {
      const puitsUtilisateur124 = puits
        .filter(puit => puit.id_utilisateur === 123)
        .map(puit => puit.id_puit);

      const mergedData = puitsUtilisateur124.map(idPuit => {
        const filteredData = stat.filter(item => item.id_puit === idPuit);

        return {
          id: idPuit,
          data: {
            labels: filteredData.map(item => item.date_stat.substring(5, 10)),
            datasets: [
              {
                data: filteredData.map(item => item.niveau_eau),
                color: (opacity = 1) => `rgba(75, 192, 192, ${opacity})`,
                strokeWidth: 2,
              },
            ],
          },
        };
      });

      setGraphData(mergedData);
    };

    fetchDataForPuits();
  }, []);

  return (
    <View style={styles.container}>
      <Text>Données des Puits pour l'Utilisateur 124</Text>
      {graphData && (
        <LineChart
          data={{
            labels: graphData[0].data.labels,
            datasets: graphData.map(puit => puit.data.datasets[0]),
          }}
          width={400}
          height={200}
          chartConfig={{
            backgroundGradientFrom: '#ffffff',
            backgroundGradientTo: '#ffffff',
            decimalPlaces: 1,
            color: (opacity = 1) => `rgba(0, 0, 0, ${opacity})`,
            labelColor: (opacity = 1) => `rgba(0, 0, 0, ${opacity})`,
            propsForDots: {
              r: '6',
              strokeWidth: '2',
              stroke: '#ffa726',
            },
          }}
          bezier
          style={{
            marginVertical: 8,
            borderRadius: 16,
          }}
          withInnerLines={false}
          withOuterLines={false}
          withDots={true}
          withShadow={false}
        />
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#1F606A',
  },
});

export default Home;
