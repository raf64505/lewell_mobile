import React, { useEffect, useState } from "react";
import { Text, View, StyleSheet, TextInput, Image, TouchableOpacity } from 'react-native';
import usersbdd from '../bdd_json/utilisateur.json'
import Ionicons from '@expo/vector-icons/Ionicons';
import { authenticate } from "../API/authApi";

const Login = ({onLogin}) => {
    const [users, setUsers] = useState([]);
    const [username, setUsername] = useState(null);
    const [pwd, setPassword] = useState(null);
    const [alertLogin, setAlertLogin] = useState(false);

    useEffect(() => {
        // Set the state with the data from the JSON file
        setUsers(usersbdd);
    }, []);

    const login = () => {
        let data = {"email": username, "password": pwd};
        authenticate(data).then(response => {
            if(response.status == 200 && response.statusText == "OK")
            {
                global.bearerToken = response.data.access_token;
                onLogin(global.bearerToken);
            }
            else
            {
                setAlertLogin(true);
            }
        }).catch(error => {
            setAlertLogin(true);
            console.log(error);
        });
    }

    return (
        <View style={styles.Login}>
            <View style={styles.blocImage}>
                <Image
                    style={styles.logo}
                    source={require('../assets/logo-leWell.png')}
                />
            </View>
            <View style={styles.blocInput}>
                <Text style={styles.label}>Email</Text>
                <TextInput
                    style={styles.input}
                    value={username}
                    onChangeText={text => setUsername(text)}
                    keyboardType='email-address'
                    autoCapitalize='none'
                />
                <Text style={styles.label}>Mot de passe</Text>
                <TextInput
                    style={styles.input}
                    value={pwd}
                    onChangeText={text => setPassword(text)}
                    secureTextEntry={true}
                    autoCapitalize='none'
                />
                <TouchableOpacity onPress={() => login()} style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center', backgroundColor: '#ccc', padding: 10, borderRadius: 5 }}>
                    <Ionicons name="lock-closed" size={20} color="#000" />
                    <Text style={{ marginLeft: 10 }}>Se connecter</Text>
                </TouchableOpacity>
                {alertLogin ? (<Text style={styles.alert}>Email ou mot de passe incorrect...</Text>) : ""}
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    Login : {
        flex: 1,
        backgroundColor: '#1F606A',
        paddingTop: 80  
    },
    input: {
        width: '80%',
        height: 40,
        borderWidth: 1,
        borderColor: 'white',
        borderRadius: 5,
        paddingHorizontal: 10,
        textAlign: 'left',
        color: 'white',
        backgroundColor: 'transparent',
        marginBottom: 20,
      },
    alert: {
        color: 'red'
    },
    button: {
        color: 'transparent',
    },
    blocImage:{
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    blocInput:{
        flex: 2,
        alignItems: 'center'
    },
    label: {
        color: 'white',
        marginBottom: 10,
    },
    logo: {
        height: 150,
        width: 150,
        marginBottom: 10,
    },
});


export default Login   