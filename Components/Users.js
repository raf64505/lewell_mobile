import React, { useEffect, useState } from "react";
import { Text, View, StyleSheet, Image, TextInput, Button } from "react-native";
import utilisateurs from "../bdd_json/utilisateur.json";
import puits from "../bdd_json/puit.json"; 
import * as FileSystem from 'expo-file-system';
import { me } from "../API/authApi";

const Users = () => {
  const [user, setUser] = useState(null);
  const [numberOfWells, setNumberOfWells] = useState(0);
  const [numberOfActiveWells, setNumberOfActiveWells] = useState(0);
  const [nom, setNom] = useState('');
  const [prenom, setPrenom] = useState('');
  const [email, setEmail] = useState('');

  useEffect(() => {

    me().then(response => {
      console.log(response);
      if(response.status == 200 && response.statusText == "OK")
      {
          console.log(response);
          setUser(response.data);
          setNom(response.data?.lastname || '');
          setPrenom(response.data?.firstname || '');
          setEmail(response.data?.email || '');
      }
    }).catch(error => {
      console.log(error);
    })

  // axios.get('http://127.0.0.1:8000/api/me', {
  //   headers: {'Authorization': 'Bearer '+global.bearerToken},
  // })
  // .then(function (response) {
  //     console.log(response);
  //     if(response.status == 200 && response.statusText == "OK")
  //     {
  //         console.log(response);
  //         setUser(response.data);
  //         setNom(response.data?.lastname || '');
  //         setPrenom(response.data?.firstname || '');
  //         setEmail(response.data?.email || '');
  //     }
  // })
  // .catch(function (error) {
  //     console.log(error);
  // });

    const user123 = utilisateurs.find(user => user.id_utilisateur === 123);

    if (user123) {
      const wellsForUser = puits.filter(well => well.id_utilisateur === user123.id_utilisateur);
      setNumberOfWells(wellsForUser.length);

      const activeWellsForUser = wellsForUser.filter(well => well.id_utilisateur === user123.id_utilisateur && well.etat === true);
      setNumberOfActiveWells(activeWellsForUser.length);
    }
  }, []);

  const handleSave = async () => {
    try {
      const updatedUser = { ...user, nom, prenom, email };
      const index = utilisateurs.findIndex(u => u.id_utilisateur === user.id_utilisateur);
      if (index !== -1) {
        utilisateurs[index] = updatedUser;
        await FileSystem.writeAsStringAsync(
          FileSystem.documentDirectory + 'utilisateur.json',
          JSON.stringify(utilisateurs)
        );
        console.log("Informations utilisateur mises à jour :", updatedUser);
      } else {
        console.log("Utilisateur non trouvé dans la base de données.");
      }
    } catch (error) {
      console.error("Erreur lors de l'enregistrement des modifications :", error);
    }
  };
  
  return (
    <View style={styles.container}>
      {user ? (
        <View style={styles.userInfo}>
          <Image source={require('../assets/logo.png')} style={styles.photo} />
          <Text style={styles.identifiants}>{user.prenom} {user.nom}</Text>
          <View style={styles.userInfoContent}>
            <View style={styles.AllText}>
              <Text style={styles.infos}>Informations Personnelles</Text>
              <View style={styles.textContainer}>
                <Text style={styles.label}>Nom : </Text>
                <TextInput
                  style={styles.textInput}
                  value={nom}
                  onChangeText={setNom}
                />
              </View>
              <View style={styles.textContainer}>
                <Text style={styles.label}>Prénom : </Text>
                <TextInput
                  style={styles.textInput}
                  value={prenom}
                  onChangeText={setPrenom}
                />
              </View>
              <View style={styles.textContainer}>
                <Text style={styles.label}>Email : </Text>
                <TextInput
                  style={styles.textInput}
                  value={email}
                  onChangeText={setEmail}
                />
              </View>
            </View>
            <View style={styles.divider} />
            <View style={styles.AllText}>
              <Text style={styles.infos}>Informations Puits</Text>
              <View style={styles.textContainer}>
                <Text style={styles.text}>Nombre de puits : {numberOfWells}</Text>
              </View>
              <View style={styles.textContainer}>
                <Text style={styles.text}>Nombre de puits actifs : {numberOfActiveWells}</Text>
              </View>
            </View>
          </View>
          <Button title="Enregistrer" onPress={handleSave} />
        </View>
      ) : (
        <Text>Loading...</Text>
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#1F606A",
    alignItems: "center", 
    justifyContent: "flex-start",
    paddingTop: 80
  },
  userInfo: {
    alignItems: "center", 
  },
  userInfoContent: {
    flexDirection: "column", 
    width: "100%", 
    paddingTop: 50,
  },
  identifiants: {
    fontWeight: "bold",
    fontSize: 35,
    marginBottom: 10, 
    color: "white", 
    textAlign:'center'
  },
  divider: {
    backgroundColor: "white",
    height: 1,
    width: "100%",
  },
  photo: {
    width: 200,
    height: 200,
    resizeMode: "cover",
    borderRadius: 100,
    marginBottom: 10, 
    filter: "invert(1)"
  },
  infos: {
    fontWeight: "bold",
    fontSize: 20,
    marginBottom: 20, 
    textDecorationLine: "underline", 
    color: "white"
  },
  AllText: {
    alignItems: "flex-start",
    justifyContent: "flex-start", 
    marginLeft: -75,
    marginTop: 15,
  },
  textContainer: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "flex-start",
    width: "100%", 
    marginBottom: 5, 
  },
  label: {
    fontWeight: "bold",
    marginRight: 10, 
    color: "white" 
  },
  text: {
    color: "white", 
    fontSize: 20,
    marginTop:10
  },
  textInput: {
    color: "white",
    fontSize: 20,
    borderWidth: 1,
    borderColor: "white",
    padding: 5,
    width: 200,
    borderRadius: 5,
  }
});

export default Users;
