import axios from 'axios';

// local
const urlServ = 'http://127.0.0.1:8000/api'

export function getStatByWell(idWell) {
    const route = '/wells/'+idWell+'/stats';
    const params = {
        headers: {
        'Authorization': 'Bearer ' + global.bearerToken
        },
    }
    return new Promise((resolve, reject) => {
        return axios.get(urlServ+route, params).then(response => {
            resolve(response)
        }).catch(error => {
            reject(error)
        });
    });
}