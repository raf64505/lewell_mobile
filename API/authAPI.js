import axios from 'axios';

// local
const urlServ = 'http://127.0.0.1:8000/api'

export function authenticate(data) {
    const route = '/login';
    return new Promise((resolve, reject) => {
        return axios.post(urlServ+route, data).then(response => {
            resolve(response)
        }).catch(error => {
            reject(error)
        });
    });
} 

export function me() {
    const route = '/me';
    const params = {
        headers: {
        'Authorization': 'Bearer ' + global.bearerToken
        },
    };
    return new Promise((resolve, reject) => {
        return axios.get(urlServ+route, params).then(response => {
            resolve(response)
        }).catch(error => {
            console.log(error)
            reject(error)
        });
    });
} 
