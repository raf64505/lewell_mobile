import axios from 'axios';

// local
const urlServ = 'http://127.0.0.1:8000/api'

export function updateUser(idUser, data) {
    const route = '/users/'+idUser;
    const params = {
        headers: {
        'Authorization': 'Bearer ' + global.bearerToken
        },
    }
    return new Promise((resolve, reject) => {
        return axios.put(urlServ+route, data, params).then(response => {
            resolve(response)
        }).catch(error => {
            reject(error)
        });
    });
}

export function deleteUser(idUser) {
    const route = '/users/'+idUser;
    const params = {
        headers: {
        'Authorization': 'Bearer ' + global.bearerToken
        },
    }
    return new Promise((resolve, reject) => {
        return axios.delete(urlServ+route, params).then(response => {
            resolve(response)
        }).catch(error => {
            console.log(error)
            reject(error)
        });
    });
} 