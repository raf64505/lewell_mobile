import axios from 'axios';

// local
const urlServ = 'http://127.0.0.1:8000/api'

export function getWellByUser(idUser) {
    const route = '/users/'+idUser+'/wells';
    const params = {
        headers: {
        'Authorization': 'Bearer ' + global.bearerToken
        },
    }
    return new Promise((resolve, reject) => {
        return axios.get(urlServ+route, params).then(response => {
            resolve(response)
        }).catch(error => {
            reject(error)
        });
    });
}

export function findWell(idWell) {
    const route = '/wells/'+idWell;
    const params = {
        headers: {
        'Authorization': 'Bearer ' + global.bearerToken
        },
    }
    return new Promise((resolve, reject) => {
        return axios.get(urlServ+route, params).then(response => {
            resolve(response)
        }).catch(error => {
            reject(error)
        });
    });
}

export function createWell(data) {
    const route = '/wells';
    const params = {
        headers: {
        'Authorization': 'Bearer ' + global.bearerToken
        },
    }
    return new Promise((resolve, reject) => {
        return axios.post(urlServ+route, data, params).then(response => {
            resolve(response)
        }).catch(error => {
            reject(error)
        });
    });
}

export function updateWell(idWell, data) {
    const route = '/wells/'+idWell;
    const params = {
        headers: {
        'Authorization': 'Bearer ' + global.bearerToken
        },
    }
    return new Promise((resolve, reject) => {
        return axios.put(urlServ+route, data, params).then(response => {
            resolve(response)
        }).catch(error => {
            reject(error)
        });
    });
}

export function deleteWell(idWell) {
    const route = '/wells/'+idWell;
    const params = {
        headers: {
        'Authorization': 'Bearer ' + global.bearerToken
        },
    }
    return new Promise((resolve, reject) => {
        return axios.delete(urlServ+route, params).then(response => {
            resolve(response)
        }).catch(error => {
            console.log(error)
            reject(error)
        });
    });
} 